// 用递归的思路实现第一道题

package main

import (
	"errors"
	"fmt"
)

func Dao(f ...float64) (float64, error) {
	if len(f) == 0 {
		return 0, errors.New("parameter length is 0")
	}

	if f[0] == 0 {
		return 0, errors.New("parameter cannot be 0")
	}
	if len(f) == 1 {
		return 1 / f[0], nil
	}

	sum, err := Dao(f[1:]...)
	if err != nil {
		return 0, err
	}

	return (1 / f[0]) * sum, nil

}

func main() {
	// i1, err := Dao()
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(i1)

	i2, err := Dao(1, 2, 3)
	if err != nil {
		panic(err)
	}
	fmt.Println(i2)

	// i3, err := Dao(1, 2, 3, 0)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(i3)

}
