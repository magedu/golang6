package main

import "fmt"

type animal interface {
	move()
}

type fish interface {
	swim()
}

type frog struct {
	name string
}

func (f frog) move() {
	fmt.Printf("%s可以爬\n", f.name)
}

func (f frog) swim() {
	fmt.Printf("%s可以在水里游\n", f.name)
}

func main() {
	var tmp = frog{name: "青蛙"}
	var a animal
	var f fish
	a = tmp
	a.move()
	f = tmp
	f.swim()
}
