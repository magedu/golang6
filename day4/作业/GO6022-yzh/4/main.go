package main

import "fmt"

func square(num interface{}) interface{} {
	switch v := num.(type) {
	case float32:
		return v * v
	case float64:
		return v * v
	case int:
		return v * v
	case byte:
		return v * v
	default:
		panic("only accept float32,float64,int,byte parameters")
	}
}

func main() {
	a1 := float32(2)
	fmt.Println(square(a1))

	a2 := float64(3)
	fmt.Println(square(a2))

	a3 := int(4)
	fmt.Println(square(a3))

	a4 := byte(5)
	fmt.Println(square(a4))

	a5 := int64(6)
	fmt.Println(square(a5))
}
