// 实现一个函数，接受若干个float64（ 用不定长参数），返回这些参数乘积的倒数，除数为0时返回error

package main

import (
	"errors"
	"fmt"
)

func Dao(f ...float64) (float64, error) {
	if len(f) == 0 {
		return 0, errors.New("parameter length is 0")
	}
	var sum = float64(1)
	for _, v := range f {
		if v == 0 {
			return 0, errors.New("parameter cannot be 0")
		}
		sum *= v
	}

	return 1 / sum, nil
}

func main() {
	// i1, err := Dao()
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(i1)

	i2, err := Dao(1, 2, 3)
	if err != nil {
		panic(err)
	}
	fmt.Println(i2)

	// i3, err := Dao(1, 2, 3, 0)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(i3)

}
