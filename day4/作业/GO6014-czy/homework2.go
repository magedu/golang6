package main

import (
	"errors"
	"fmt"
)

func multiply(args ...float64) float64 {

	n := len(args)
	if n == 1 {
		return args[0]
	}

	return args[n-1] * multiply(args[:n-1]...)
}


func reverseMultiply2(args ...float64) (float64, error) {
	result := multiply(args...)
	if result == 0 {
		return -1, errors.New("Error: the result of multiplication cannot be 0 when the result is divided")
	}
	return 1 / result, nil

}
func main() {
	args := []float64{23.33, 33.33, 43.33, 54.33}
	fmt.Println(reverseMultiply2(args...))
}
