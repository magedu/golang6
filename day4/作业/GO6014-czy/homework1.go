package main

import (
	"errors"
	"fmt"
)

func reverseMultiply(args ...float64) (float64, error) {
	var result float64 = 1
	for _, v := range args {
		result *= v
	}
	if result == 0 {
		return -1, errors.New("Error: the result of multiplication cannot be 0 when the result is divided")
	}
	return 1 / result, nil

}

func main() {
	args := []float64{23.33, 33.33, 43.33, 54.33}
	fmt.Println(reverseMultiply(args...))
}
