package main

import "fmt"

func square(num interface{}) interface{} {
	// 允许是4种类型：float32、float64、int、byte
	switch value := num.(type) {
	case float32:
		return value * value
	case float64:
		return value * value
	case int:
		return value * value
	case byte:
		return value * value
	default:
		return "Invaid type of value"
	}
}

func main() {
	var num interface{} = 2
	fmt.Println(square(num))
}
