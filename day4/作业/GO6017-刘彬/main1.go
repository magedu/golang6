package main

import (
	"errors"
	"fmt"
)

func reciprocal(data ...float64) (float64, error) {
	if len(data) == 0 {
		return -1,errors.New("没有参数传入。")
	}
	product := float64(1)
	for i := 0; i < len(data); i++ {
		product *= data[i] //采用迭代的方式求切片所有数的乘积
	}
	if product == 0 {
		return -1, errors.New("不能对零求倒数。") //errors.New返回格式化为给定文本的error
	}
	return 1 / product, nil
}

func main() {
	data := []float64{1, 2, 3}
	if v, err := reciprocal(data...); err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(v)
	}
}
