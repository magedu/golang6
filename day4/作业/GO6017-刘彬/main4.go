package main

import (
	"fmt"
	"strconv"
)

func square(num interface{}) interface{} {
	switch v := num.(type) { //switch type语句实现类型断言
	case float32:
		s := []byte("The type of number is float32,its square value is ")
		s = strconv.AppendFloat(s, float64(v*v), 'f', 4, 32) //strconv.AppendFloat将浮点数的字符串形式添加到指定的[]byte切片中
		s = append(s, '.')                                   //添加"."作为语句结束标志
		return string(s)
	case float64:
		s := []byte("The type of number is float64,its square value is ")
		s = strconv.AppendFloat(s, v*v, 'f', 4, 32)
		s = append(s, '.')
		return string(s)
	case int:
		s := []byte("The type of number is int,its square value is ")
		s = strconv.AppendInt(s, int64(v*v), 10) //strconv.AppendInt将整型数的字符串形式添加到指定的[]byte切片中
		s = append(s, '.')
		return string(s)
	case byte:
		s := []byte("The type of number is byte,its square value is ")
		s = strconv.AppendInt(s, int64(v*v), 10)
		s = append(s, '.')
		return string(s)
	default:
		return "undefined type"
	}
}

func main() {
	a := square(4.3)
	fmt.Println(a)
}
