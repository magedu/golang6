package main

import (
	"errors"
	"fmt"
)

/*
实现一个函数，接受若干个float64（用不定长参数）
返回这些参数乘积的倒数，除数为0时返回error
用递归实现
*/

func plusBottom(args ...float64) (float64, error) {
	if len(args) == 0 {
		return 1.0, nil
	}
	first := args[0]
	if first == 0 {
		return 1, errors.New("divide by zero")
	}
	if len(args) == 1 {
		return 1.0 / first, nil
	}
	remain := args[1:]
	result, err := plusBottom(remain...)
	if err != nil {
		return 1, err
	} else {
		return 1.0 / first * result, nil
	}
}

func main() {
	args := []float64{1.3, 2.5, 3.7} // plusBottom(1.3, 2.5, 3.7)=1/1.3*plusBottom(2.5, 3.7)=1/1.3*1/2.5*plusBottom(3.7)=1/1.3*1/2.5*1/3.7
	//res, err := plusBottom(1.3, 2.5, 3.7)
	res, err := plusBottom(args...)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("result=%f\n", res)
	}
}
