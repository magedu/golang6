package main

import (
	"errors"
	"fmt"
	// "unsafe"
)

type error interface {
	Error() string
}

func work1(args ...float64) (float64, error) {
	var num float64 = 1
	fmt.Printf("%v,%T\n",num,num)
	for _,i := range args {
		fmt.Printf("%v,%T\n",i,i)
		num *= i
	}
	if num == 0 {
		return 1/num, errors.New("it's zero")
	}else {
		return 1/num, nil
	}
	
}

func main() {
	res,err := work1(1.3,0,3.1,4.2,5.6)
	
	if err != nil {
		fmt.Printf("%v",err)
	}else {
		fmt.Printf("%v",res)
	}
}