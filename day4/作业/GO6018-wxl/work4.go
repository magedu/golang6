package main

import (
	"fmt"
	"reflect"
)

func square(num interface{}) interface{} {

	str1 := reflect.TypeOf(num).String()
	fmt.Printf("%T\n",str1)

	switch str1 {
		case "float32":
			return num
		case "float64":
			return num
		case "int":
			return num
		case "byte":
			return num 
		default:
			panic("")
	}
}

func main() {
	a1 := float32(2)
	fmt.Println(square(a1))

// 	a2 := float64(3)
// 	fmt.Println(square(a2))

// 	a3 := int(4)
// 	fmt.Println(square(a3))

// 	a4 := byte(5)
// 	fmt.Println(square(a4))

// 	a5 := int64(6)
// 	fmt.Println(square(a5))
}