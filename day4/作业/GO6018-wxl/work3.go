package main

import (
	"fmt"
)

type fish interface {
	swiming()
}

type Reptiles interface {
	climb()
}

type frog struct {

}

func (f frog) swiming() {
	fmt.Println("it can swiming!")
}

func (f frog) climb() {
	fmt.Println("it can climb!")
}

func main() {
	var x fish
	var y Reptiles
	f1 := frog{}
	x = f1
	y = f1
	x.swiming()
	y.climb()
}