package main

import (
	"errors"
	"fmt"
	// "unsafe"
)

type error interface {
	Error() string
}

func work2(args ...float64) (float64, error) {
	if len(args) == 1 {
		return 1,nil
	}

	num,err := work2(args[1:]...)
	if num == 0 {
		return 1/(args[0]*num),errors.New("it's zero")
	}else {
		return 1/(args[0]*num),err
	}
		
}

func main() {
	res,err := work2(1.3,0,3.1,4.2,5.6)
	
	if err != nil {
		fmt.Printf("%v",err)
	}else {
		fmt.Printf("%v",res)
	}
}