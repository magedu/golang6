// 输出一个int32对应的二进制数

package main

import (
	"fmt"
	"math"
	"strings"
)

func BinaryFormat(n int64) string {

	sb := strings.Builder{}
	a := int64(math.Pow(2, 31))
	for i := 0; i < 32; i++ {
		if n&a != 0 {
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		a >>= 1
	}

	return sb.String()

}


func main() {

	res := BinaryFormat(180)
	fmt.Println(res)

}