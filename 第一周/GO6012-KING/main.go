package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// 99乘法表
	for i := 1; i < 10; i++ {
		for j := 1; j < i+1; j++ {
			fmt.Printf("%d * %d = %2d  ", j, i, i*j)
		}
		fmt.Println()
	}

	// 猜数字
	fmt.Println("--------------------------")
	rand.Seed(time.Now().Unix())
	sum := rand.Intn(100)
	var x int
	for i := 0; i < 5; i++ {
		fmt.Print("输入一个数字：")
		fmt.Scan(&x)
		if sum > x {
			fmt.Println("小了")
		} else if sum < x {
			fmt.Println("大了")
		} else {
			fmt.Println("猜对了")
		}
	}
	fmt.Printf("%T, %d", sum, sum)
}
