package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	fmt.Println(BinaryFormat(8))
}

func BinaryFormat(n int32) string {
	a := uint32(n)
	sb := strings.Builder{}
	c := uint32(math.Pow(2, 31))
	for i := 0; i < 32; i++ {
		if a >= c {
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		a <<= 1
	}
	return sb.String()
}
