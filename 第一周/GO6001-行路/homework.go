
package main
import (
"fmt"
"math"
"strings"
)

//输出一个int32对应的二进制表示
//通过main函数调用BinaryFormat，并看0、1、-1、230、-230对应的二进程表示
func BinaryFormat(n int32) string {
	sb := strings.Builder{}
	c := int32(math.Pow(2, 31))
	for i := 0; i < 32; i++ {
		if n&c != 0 {
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		c >>= 1 //每循环一次高位的1向右移动一位
	}
	return sb.String()
}

func main() {
	fmt.Println(BinaryFormat(0))
	fmt.Println(BinaryFormat(1))
	fmt.Println(BinaryFormat(-1))
	fmt.Println(BinaryFormat(260))
	fmt.Println(BinaryFormat(-260))
}
