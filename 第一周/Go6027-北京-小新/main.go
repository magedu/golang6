package main

import (
	"fmt"
	"math"
	"strings"
)

//输出一个int32对应的二进制表示

func BinaryFormat(n int32) string {
	a := uint32(n)
	sb := strings.Builder{}
	c := uint32(math.Pow(2, 31))
	for i := 0; i < 32; i++ {
		if a&c != 0 {
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		c >>= 1
	}
	return sb.String()
}

func main() {
	// 完成两行填空，然后写一个main函数调用BinaryFormat，看一下0、1、-1、260、-260对应的二进程表示是什么
	var d = []int32{0, 1, 260, -260}
	for _, value := range d {
		fmt.Printf("%d 二进制: %s\n", value, BinaryFormat(value))
	}
}
