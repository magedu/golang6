package main

import (
	"fmt"
	"strings"
)

func arr2string(arr []int) string {
	var str strings.Builder
	for _, value := range arr {
		fmt.Fprint(&str, value)
	}
	return str.String()
}

func main() {
	var arr1 []int = []int{2, 4, 6}
	fmt.Println(arr2string(arr1))
}

// 未使用空格分隔字符