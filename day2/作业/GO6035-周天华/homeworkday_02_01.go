package main

import (
	"fmt"
	"math/rand"
)

func main() {

	//作业一
	//声明创建切片以及定义类型
	var sliceInt []int
	//初始化切片长度为0，容量为10
	sliceInt = make([]int, 0, 10)
	//设置一个for循环，执行一百次调用rand.Intn(128)进行添加元素
	for i := 0; i < 100; i++ {
		//sliceInt使用append添加rand.Intn(128)的随机元素
		sliceInt = append(sliceInt, rand.Intn(128))
	}
	fmt.Println("sliceInt切片的元素为：", sliceInt)
	fmt.Println("sliceInt切片的元素个数为：", len(sliceInt))

	var m1 = make(map[int]int, 100)
	for _, w := range sliceInt {
		if _, ok := m1[w]; !ok {
			m1[w] = 0 //默认的零值就是0，所以这一步是多余的
		} else {
			m1[w]++
		}
	}

	fmt.Println()
	fmt.Println() //空行
	fmt.Print("不同的元素为：")
	for key := range m1 {
		fmt.Print(` `, key)
	}
	fmt.Println() //空行
	fmt.Println() //空行
	fmt.Println("统计切片中有多少个不同的元素：", len(m1))
}

// 结果是对的，可以尝试使用函数将功能独立出来