package main

import (
	"fmt"
	"strings"
)

func main() {
	//test nill slice
	str1 := arr2string([]int{})
	for _, s := range str1 {
		fmt.Printf("%c ", s)
	}
	fmt.Println()
	//test normal slice
	str2 := arr2string([]int{1, 2, 3})
	for _, s := range str2 {
		fmt.Printf("%c ", s)
	}
}

func arr2string(arr []int) string {
	if len(arr) == 0 {
		return ""
	}
	var str strings.Builder
	for _, value := range arr {
		fmt.Fprint(&str, value)
	}
	return str.String()
}
