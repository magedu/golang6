package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

func main() {
	job1()
	fmt.Println(arr2string([]int{2, 4, 6}))
}

func job1() {
	rand.Seed(time.Now().Unix())

	s1 := make([]int, 10)
	for i := 0; i < 100; i++ {
		s1 = append(s1, rand.Intn(11))
	}

	m1 := make(map[int]int)
	for _, v := range s1 {
		m1[v] = m1[v] + 1
	}

	for k, v := range m1 {
		fmt.Printf("该切片中共有 %d 个 %d\n", v, k)
	}

}
// 需要返回的是map里键的数量，不是单独统计每个键出现的次数

func arr2string(arr []int) string {
	s1 := []string{}
	for _, v := range arr {
		s1 = append(s1, strconv.Itoa(v))
	}

	return strings.Join(s1, ",")
}

//使用空格来间隔，不是逗号