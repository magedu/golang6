

1.创建一个初始长度为0、容量为10的int型切片，调用rand.Intn(128)100次，
往切片里面添加100个元素，利用map统计该切片里有多少个互不相同的元素。

package main

import (
	"fmt"
	"math/rand"
)

//包 rand 实现了一个加密安全的随机数生成器

func main() {
//定义一个长度为0，容量维10的变量
	var length = make([]int, 0, 10)

//往变量里面追加rand生成的随机数
	for i := 0; i < 100; i++ {
		random := rand.Intn(128)
		length = append(length, random)
		//fmt.Println(random)
	}
	fmt.Println(length)

	//定义map

	//var ars int = 0

	//定义map weight 输出length中数值和重复的次数
	var weight = make(map[int]int)

	for ars := 0; ars < 100; ars++ {
		var a int = 0
		for _, i := range length {
			if i == length[ars] {
				//var a int=0
				a++
				weight[i] = a
			}
		}
	}

//输出weight中key的值，也就是不重复的部分 并输出不重复的数值
	var weight2 = make([]int, 0, 10)
	//遍历map weight，输出key也就是不重复的部分
	for key, _ := range weight {
		weight2 = append(weight2, key)
	}
	fmt.Println(weight2)
	fmt.Println(len(weight2))
}

// 统计重复次数时，使用的双重循环效率太低，可以利用map的键值对访问方式来进行判断。
// 即当键key存在时，dict['key']返回value,true，当键不存在时，返回0,false







2.实现一个函数func arr2string(arr []int) string，比如输入[]int{2,4,6}，返回“2 4 6”。输入的切片可能很短，也可能很长。
package main

import (
	"fmt"
	"strings"
)

func main() {
	var b strings.Builder
	var a []int = []int{1, 2, 3, 4, 5}
	for _, i := range a {
		fmt.Fprintf(&b, "%d...", i)
		
	}
	b.WriteString("ignition")
	fmt.Println(b.String())
}

// 方法和结果都是对的，可以想一下还有没有其他方式？


