package main

import (
	"fmt"
	"math/rand"
	"strings"
)

func countSliceValue(s []int) map[string]int {
	mp := make(map[string]int, len(s))
	for _, sliceValue := range s {
		sliceValue := fmt.Sprint(sliceValue)
		_, ok := mp[sliceValue]
		if ok {
			mp[sliceValue]++
		} else {
			mp[sliceValue] = 1
		}
	}
	return mp
}

func arr2string(arr []int) string {
	sb := strings.Builder{}
	for i, v := range arr {
		sb.WriteString(fmt.Sprint(v))
		if i != len(arr)-1 {
			sb.WriteString(" ")
		}
	}
	return sb.String()
}

func main() {

	// 1.创建一个初始长度为0、容量为10的int型切片，调用rand.Intn(128)100次，往切片里面添加100个元素，利用map统计该切片里有多少个互不相同的元素。

	// 创建切片
	slice := make([]int, 0, 10)
	for i := 0; i < 100; i++ {
		slice = append(slice, rand.Intn(128))
	}

	// 去重并统计次数
	for k, v := range countSliceValue(slice) {
		fmt.Printf("%v: %d\n", k, v)
	}
	fmt.Printf("以上是元素出现次数统计，总共 %d 个。\n", len(countSliceValue(slice)))
	fmt.Println("-------------------------")

	// 2.实现一个函数func arr2string(arr []int) string，比如输入[]int{2,4,6}，返回“2 4 6”。输入的切片可能很短，也可能很长。
	s1 := []int{2, 4, 6}
	fmt.Println(arr2string(slice))
	fmt.Println(arr2string(s1))
}

//实现的非常棒，掌握的非常好了。