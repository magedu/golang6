package hw

import (
	"math/rand"
)

// GetNumOfDiffEle Homework for 20210925.
// 将一个有 100 个随机数字的切片, 去重后计算长度.
func GetNumOfDiffEle() int {
	s := generateSlice()
	rs := duplicateRemove(s)
	// fmt.Println(s)
	// fmt.Println(rs)
	// fmt.Println(len(s))
	// fmt.Println(len(rs))
	return len(rs)
}

// 这个函数可以将切片长度作为参数传入，实现更灵活的创建不同长度切片对象
func generateSlice() []int {
	var s = make([]int, 0, 10)
	for i := 0; i < 100; i++ {
		s = append(s, rand.Intn(128))
	}
	return s
}

func duplicateRemove(s []int) []int {
	rs := []int{}

	// nset := make(map[int]bool)
	nset := map[int]bool{}
	for _, v := range s {
		if _, exist := nset[v]; !exist {
			rs = append(rs, v)
			nset[v] = true
		} else {
			// fmt.Printf("%d ", v)
		}
	}
	return rs
}

// 变量命令需要尽量体现出变量的含义, 第二个函数可以直接返回map的长度