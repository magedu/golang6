//统计元素个数
package main

import (
	"fmt"
	"math/rand"
)

var sum = 0

func main() {
	a := make([]int, 0, 10)
	for i := 0; i < 100; i++ {  
		a = append(a, rand.Intn(128))
	}

	b := make(map[int]int, 200)
	for _, v := range a {  //将切片a中的元素作为map的key，并统计每个key出现的次数
		if _, ok := b[v]; !ok {
			b[v] = 0 //初值就是0，不需要这个
			b[v]++
		} else {
			b[v]++
		}
	}

	fmt.Println(len(b))
	fmt.Println(b)
}

//切片转换成字符串
package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

func genslice(n int) []int {  //定义一个可以生成n个int元素的切片的函数
	arr := make([]int, 0)
	for i := 0; i < n; i++ {
		arr = append(arr, rand.Intn(200))
	}
	return arr
}

func arr2string(arr []int) string {  //定义切片转换为字符串的函数
	s := strings.Builder{}
	for _, v := range arr {
		s.WriteString(strconv.Itoa(v))
		s.WriteString(" ")
	}
	return strings.Trim(s.String(), " ")
}

func main() {
	n := 6
	sli := genslice(n)
	fmt.Printf("%#v\n", sli)
	str := arr2string(sli)
	fmt.Println(str)
}

// 结果是对的，变量命名要尽量符合它的含义，不要使用abc这种，
// 函数尽量都有返回值，不要打印返回值