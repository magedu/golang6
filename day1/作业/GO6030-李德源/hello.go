package main

import (
	"fmt"
	"hello/day1"
)

func main() {
	fmt.Println(day1.BinaryFormat(0))
	fmt.Println(day1.BinaryFormat(1))
	fmt.Println(day1.BinaryFormat(-1))
	fmt.Println(day1.BinaryFormat(260))
	fmt.Println(day1.BinaryFormat(-260))
}

//方法和结果都是对的，可以尝试一下使用另外的位运算方式来实现