package main

import (
	"fmt"
	"math"
	"strings"
)

func BinaryFormat(n int32) string {
	a := uint32(n)
	// fmt.Printf("%b\n",a)
	sb := strings.Builder{}
	c := uint32(math.Pow(2,31))
	for i := 0; i < 32; i++ {
		if a&c > 0 {
			sb.WriteString("1")
		}else{
			sb.WriteString("0")
		}
		c >>= 1
	}
	return sb.String()
}



func main() {
	var nums = [5]int32{0,1,-1,260,-260}
	for _ ,num := range nums {
		fmt.Printf("%d binary is %s by func\n",num,BinaryFormat(num))
	}


}

//方法和结果都是对的，可以尝试一下使用另外的位运算方式来实现