package utils

import (
	"math"
	"strings"
)

//输出一个int32对应的二进制表示
func BinaryInt32(n int32) string {
	a := uint32(n)
	sb := strings.Builder{}
	c := uint32(math.Pow(2, 31))
	for i := 0; i < 32; i++ {
		if c&a != 0 {
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		c >>= 1
	}
	return sb.String()

}

//输出一个int对应的二进制表示
func BinaryInt(n int) string {
	a := uint(n)
	sb := strings.Builder{}
	c := uint(math.Pow(2, 63))
	// fmt.Printf("c %b\n", c)
	for i := 0; i < 64; i++ {
		if c&a != 0 {
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		c >>= 1
	}
	return sb.String()

}
