package main

import (
	"fmt"
	"trans/utils"
)

func tansInt() {
	slice := []int{0, 1, -1, 5, -5, 10, -10, 185, -185, 256, -256, 260, -260, 1024, -1024}
	for _, v := range slice {
		fmt.Printf("%d对应的二进制表示为%s\n", v, utils.BinaryInt(v))
	}

}

func transInt32() {
	slice := []int32{0, 1, -1, 5, -5, 10, -10, 185, -185, 256, -256, 260, -260, 1024, -1024}
	for _, v := range slice {
		fmt.Printf("%d对应的二进制表示为%s\n", v, utils.BinaryInt32(v))
	}

}
func main() {
	tansInt()
	transInt32()
}

// 结果是对的，可以试一下不使用库函数来自己实现，这题主要考察对二进制运算的熟悉程度。
