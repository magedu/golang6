package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	//调用BinaryFormat，看一下0、1、-1、260、-260对应的二进制表示是什么

	var n int32 = 0
	c := BinaryFormat(n)
	fmt.Printf("%d:%c\n", n, c)

	n = 1
	c = BinaryFormat(n)
	fmt.Printf("%d:%c\n", n, c)

	n = -1
	c = BinaryFormat(n)
	fmt.Printf("%d:%c\n", n, c)

	n = 260
	c = BinaryFormat(n)
	fmt.Printf("%d:%c\n", n, c)

	n = -260
	c = BinaryFormat(n)
	fmt.Printf("%d:%c\n", n, c)
}

//输出一个int32对应的二进制表示
func BinaryFormat(n int32) string {
	a := uint32(n)
	sb := strings.Builder{}
	c := uint32(math.Pow(2, 31))
	for i := 0; i < 32; i++ {
		if a&c != 0 { //与运算
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		c >>= 1 //向右位移1位，...最后为0
	}
	return sb.String()
}

//方法和结果都是对的，可以尝试一下使用另外的位运算方式来实现