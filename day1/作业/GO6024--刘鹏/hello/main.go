package main

import (
	"fmt"
	"go/test"
)

func main() {
	fmt.Println(test.BinaryFormat(0), "\n", test.BinaryFormat(1))
	fmt.Println(test.BinaryFormat(-1))
	fmt.Println(test.BinaryFormat(-260))
	fmt.Println(test.BinaryFormat(260))
}

//方法和结果都是对的，可以尝试一下使用另外的位运算方式来实现