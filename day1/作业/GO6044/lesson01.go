package main

import (
	"fmt"
	"math"
	"strings"
)

func BinaryFormat(n int32) string {
	a := uint32(n) // 把 int32 转换为无符号的 uint32，用于跟 uint32 最大值位逐个对比匹配
	fmt.Println("uint32 of n:", a)

	sb := strings.Builder{} // 初始化一个字符串方法来存储匹配的位
	// string.Builder{} 通过 slice 存储，通过 WriteString 的写入方法来写入内容，然后通过调用 String() 方法来获取拼接的字符串
	c := uint32(math.Pow(2, 31)) // 最高位上是1，其他位全是0
	for i := 0; i < 32; i++ {
		if a&c != 0 { // 判断n的当前位上是否为1
			sb.WriteString("1")
		} else {
			sb.WriteString("0")
		}
		c >>= 1 // 每循环一次高位的"1"往右移一位
	}
	return sb.String()
}

func main() {
	fmt.Println(BinaryFormat(0))
	fmt.Println(BinaryFormat(1))
	fmt.Println(BinaryFormat(-1))
	fmt.Println(BinaryFormat(260))
	fmt.Println(BinaryFormat(-260))
}

// 完成两行填空，然后写一个main函数调用BinaryFormat，看一下0、1、-1、260、-260对应的二进程表示是什么
