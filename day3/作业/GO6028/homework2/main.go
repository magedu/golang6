package main

import "fmt"

/*
给定月份，判断属于哪个季节。分别用if和switch实现
*/

func season(month int) string {
	if month <=0 {
		fmt.Printf("非法月份%d\n", month+1)
		return ""
	}
	month -= 1
	d := month / 3
	if d == 0 {
		return "春"
	} else if d == 1 {
		return "夏"
	} else if d == 2 {
		return "秋"
	} else if d == 3 {
		return "冬"
	} else {
		fmt.Printf("非法月份%d\n", month+1)
		return ""
	}
}

func season2(month int) string {
	if month <=0 {
		fmt.Printf("非法月份%d\n", month+1)
		return ""
	}
	month -= 1
	switch month / 3{
	case 0:
		return "春"
	case 1:
		return "夏"
	case 2:
		return "秋"
	case 3:
		return "冬"
	default:
		fmt.Printf("非法月份%d\n", month+1)
		return ""
	}
}
// 这两种实现都有问题，如果输入的month大于12，会是什么情况？

func main() {
	for i := -2; i < 15; i++ {
		fmt.Printf("month %d season %s\n", i, season2(i))
	}
}
