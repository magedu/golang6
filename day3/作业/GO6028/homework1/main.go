package main

/*
随机初始化两个8*5的矩阵，求两个矩阵的和（逐元素相加）
*/

import "math/rand"

func genMatrix() [8][5]int {
	matrix := [8][5]int{}
	for i := 0; i < 8; i++ {
		for j := 0; j < 5; j++ {
			matrix[i][j] = rand.Intn(100)
		}
	}
	return matrix
}

func matrixAdd(m1, m2 [8][5]int) [8][5]int {
	sum := [8][5]int{}
	for i := 0; i < 8; i++ {
		for j := 0; j < 5; j++ {
			sum[i][j] = m1[i][j] + m2[i][j]
		}
	}
	return sum
}
