package main

import (
	"fmt"
	"math/rand"
	"time"
)

type student struct { //定义student结构体
	name                     string
	philology, math, english int
}

func genStu(name string, philology, math, english int) *student { //结构体student的构造函数
	return &student{
		name:      name,
		philology: philology,
		math:      math,
		english:   english,
	}
}

func genSliStu(n int) []student { //生成student结构体切片，包含n个student结构体
	stusli := make([]student, 0)
	for i := 0; i < n; i++ {
		name := fmt.Sprintf("stu%d", i)
		stusli = append(stusli, *genStu(name, rand.Intn(61)+40, rand.Intn(61)+40, rand.Intn(61)+40)) //设置每门科目最低分为40，满分100分
	}
	return stusli
}

func main() {
	rand.Seed(time.Now().Unix()) //设置随机数种子
	s := genSliStu(5)
	fmt.Println(s)

	num := 0
	for _, v := range s {
		ave := (v.philology + v.math + v.english) / 3 //求student三门课的平均分
		if ave < 60 {
			fmt.Printf("%s的平均分为：%d，平均分小于60分。\n", v.name, ave)
			num++ //统计平均分小于60分的student个数
		} else {
			fmt.Printf("%s的平均分为：%d\n", v.name, ave)
		}
	}
	fmt.Printf("平均分小于60分的同学有%d位。\n", num)

	sum1, sum2, sum3 := 0, 0, 0
	for i := 0; i < len(s); i++ {
		sum1 += s[i].philology
	}
	ave1 := sum1 / len(s)

	for i := 0; i < len(s); i++ {
		sum2 += s[i].math
	}
	ave2 := sum2 / len(s)

	for i := 0; i < len(s); i++ {
		sum3 += s[i].english
	}
	ave3 := sum3 / len(s)
	fmt.Printf("全班平均分：语文的平均分为%d，数学平均分为%d，英语平均分为%d。\n", ave1, ave2, ave3)
}
// 代码实现的不错，结果也是对的，但是变量取名尽量使用有含义的，不要直接使用单个字母，这样时间久了再回来
// 看代码，自己可能就很容易模糊。
