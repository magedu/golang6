package main

import "fmt"

// 给定月份，判断属于哪个季节，分别用if和switch实现

func JudgeSeason1(month int) (string, error) {
	switch month {
	case 1, 2, 3:
		return "春", nil
	case 4, 5, 6:
		return "夏", nil
	case 7, 8, 9:
		return "秋", nil
	case 10, 11, 12:
		return "冬", nil
	default:
		err := fmt.Errorf("输入错误。")
		return "", err
	}
}

func JudgeSeason2(month int) (string, error) {
	if month >= 1 && month <= 3 {
		return "春", nil
	} else if month >= 4 && month <= 6 {
		return "夏", nil
	} else if month >= 7 && month <= 9 {
		return "秋", nil
	} else if month >= 10 && month <= 12 {
		return "冬", nil
	} else {
		err := fmt.Errorf("输入错误。")
		return "", err
	}
}

func main() {
	var input int
	fmt.Print("输入月份：")
	fmt.Scan(&input)
	season, err := JudgeSeason2(input)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%d月份属于%s天\n", input, season)
}
