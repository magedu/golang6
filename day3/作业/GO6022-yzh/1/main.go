package main

// 随机初始化两个8*5的矩阵，求两个矩阵的和（逐元素想加）

import (
	"fmt"
	"math/rand"
	"time"
)

func GenArr() [8][5]int {
	var arr = [8][5]int{}
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 8; i++ {
		for j := 0; j < 5; j++ {
			arr[i][j] = rand.Intn(100)
		}
	}
	return arr
}

func PrintArr(arr [8][5]int) {
	for _, v1 := range arr {
		for _, v2 := range v1 {
			fmt.Printf("%d ", v2)
		}
		fmt.Println()
	}
}

func main() {
	arr1 := GenArr()
	fmt.Println("-------------arr1-------------")
	PrintArr(arr1)

	arr2 := GenArr()
	fmt.Println("-------------arr2-------------")
	PrintArr(arr2)

	var sum [8][5]int
	for i1, v1 := range arr1 {
		for i2, v2 := range v1 {
			sum[i1][i2] = v2 + arr2[i1][i2]
		}
	}

	fmt.Println("-------------sum-------------")
	for i1, v1 := range sum {
		for i2, v2 := range v1 {
			fmt.Printf("%d[%d+%d] ", v2, arr1[i1][i2], arr2[i1][i2])
		}
		fmt.Println()
	}

}
// 变量命名不要用这种很容易把自己搞混的方式，要使用有意义的，例如idx、val
