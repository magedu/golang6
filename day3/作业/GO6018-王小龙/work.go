package main

import (
	"fmt"
	"math/rand"
)

func CreateMatrix() [8][5]int {
	var ma [8][5]int
	for i := 0; i < 8; i++ {
		for j := 0; j < 5; j++ {
			ma[i][j] = rand.Intn(128)
		}
	}
	return ma
}

func AddMatrix(res1,res2 [8][5]int) [8][5]int {
	var ma [8][5]int
	for i := 0; i < 8; i++{
		for j := 0; j < 5; j++ {
			ma[i][j] = res1[i][j] + res2[i][j]
		}
	}
	return ma
}

func SearchSeasonsForSwitch(num int) string {
	switch num {
		case 1:
			return "冬"
		case 2:
			return "冬"
		case 3:
			return "春"
		case 4:
			return "春"
		case 5:
			return "春"
		case 6:
			return "夏"
		case 7:
			return "夏"
		case 8:
			return "夏"
		case 9:
			return "秋"
		case 10:
			return "秋"
		case 11:
			return "秋"
		case 12:
			return "冬"
		default:
			return "输入错误，请重新输入"
	}
}
// 可以把多个相同的值合并到一起

func SearchSeasonsForIf(num int) (res string) {
	if num >= 3 && num <= 5{
		return "春"
	}else if num >= 6 && num <= 8 {
		return "夏"
	}else if num >= 9 && num <= 11 {
		return "秋"
	}else if num >= 12 && num <= 2 {
		return "冬"
	}else {
		return "输入错误，请重新输入"
	}
}


type Student struct {
	name string
	chinese int
	math int
	english int
}

func main() {
	// 作业一
	fmt.Println("作业一-------------------------------------------------------------------")
	res1 := CreateMatrix()
	res2 := CreateMatrix()
	fmt.Printf("第一个矩阵\n%v\n",res1)
	fmt.Printf("第二个矩阵\n%v\n",res2)
	fmt.Printf("两个举证求和\n%v\n",AddMatrix(res1,res2))

	// 作业二
	fmt.Println("作业二-------------------------------------------------------------------")
	fmt.Println("请输入您要查询月份的数字")
	var num int
	fmt.Scanln(&num)
	fmt.Printf("%d月是%s天\n",num,SearchSeasonsForSwitch(num))
	fmt.Printf("%d月是%s天\n",num,SearchSeasonsForIf(num))

	// 作业三
	fmt.Println("作业三-------------------------------------------------------------------")
	var People []Student
	P1 := Student{
		name: "张三",
		chinese: 98,
		math: 100,
		english: 34,
	}
	P2 := Student{
		name: "李四",
		chinese: 40,
		math: 50,
		english: 60,
	}
	P3 := Student{
		name: "王五",
		chinese: 87,
		math: 72,
		english: 54,
	}
	P4 := Student{
		name: "赵六",
		chinese: 98,
		math: 54,
		english: 99,
	}
	People = append(People,P1,P2,P3,P4)
	fmt.Println(People)
	TotalNum := 0
	i := 0
	for _,v := range People {
		AvgNum := (v.chinese+v.english+v.math)/3
		fmt.Printf("%s的平均成绩为%d\n",v.name,AvgNum)
		TotalNum += AvgNum
		if AvgNum < 60 {
			i += 1
		}
	}
	fmt.Printf("班级平均成绩为%d\n",TotalNum/len(People))
	fmt.Printf("全班同学平均份低于60的有%d位",i)

}
// 学生对象也可以尝试使用随机数来赋值创建，尝试一下