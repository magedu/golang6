package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

/*
#########作业第一题

import (
	"fmt"
	"math/rand"
)

//生成8*5的随机数组
func Gen_Array() [8][5]int {
	rand_array := [8][5]int{}
	for i := 0; i < 8; i++ {
		for j := 0; j < 5; j++ {
			rand_array[i][j] = rand.Int()
			fmt.Println()
		}
	}
	return rand_array
}

//计算两个矩阵的和
func Array_Sum(a, b [8][5]int) [8][5]int {
	sum := [8][5]int{}
	for i := 0; i < 8; i++ {
		for j := 0; j < 5; j++ {
			sum[i][j] = a[i][j] + b[i][j]
		}
	}
	return sum
}

func main() {
	array1 := Gen_Array()
	array2 := Gen_Array()
	array3 := Array_Sum(array1,array2)
	fmt.Println(array3)

}
*/

/*
#######第二题作业
//if判断方式
func Jude_if(m int) string{
	if m >=10{
		return "冬季"
	}else if m >=8 {
		return "秋季"
	}else if m >= 4 {
		return "夏季"
	}else {
		return "春季"
	}
}
//switch判断方式
func Jude_switch(m int) string{
	switch m{
	case 1,2,3:
		return "春季"
	case 4,5,6:
		return "夏季"
	case 7,8,9:
		return "秋季"
	case 10,11,12:
		return "冬季"
	default:
		return "请检查是否输入了数字[1-12]"
	}

}

func main() {
	fmt.Println(Jude_if(9))
	fmt.Println(Jude_switch(2))
}
*/

//作业第三题
type Student struct {
	Name    string
	Course1 float32
	Course2 float32
	Course3 float32
}

func (f Student) Person_sum_avg() (float32, float32) {
	var person_sum float32
	var person_avg float32
	person_sum += f.Course1 + f.Course2 + f.Course3
	person_avg = person_sum / 3
	return person_sum, person_avg
}

//生成学生
func Gen_student(n int) []Student {
	rand.Seed(time.Now().Unix())
	class := make([]Student, 0)
	for i := 0; i < n; i++ {
		class = append(class, Student{
			Name:    "student" + strconv.Itoa(i),
			Course1: float32(rand.Intn(100)),
			Course2: float32(rand.Intn(100)),
			Course3: float32(rand.Intn(100)),
		})
	}
	return class
}

func main() {
	class1 := Gen_student(10)
	fail_student := make([]string,0)
	var all_sum float32
	for _, value := range class1 {
		person_sum, person_avg := value.Person_sum_avg()
		fmt.Printf("%s的总分是%g，平均分是%g\n", value.Name, person_sum, person_avg)
		if person_avg <60{
			fail_student = append(fail_student, value.Name)
		}
		all_sum += person_sum + all_sum // 使用了+=，后面为啥还要加all_sum
	}
		fmt.Printf("全班级的总分是%g,平均分是%g\n", all_sum, all_sum/float32(len(class1)))
		fmt.Printf("不及格的同学名单如下：%v\n",fail_student)
}
// 输出不及格学生人数，不是名单
