package main

import (
	"fmt"
	"math/rand"
)

const (
	x = 8
	y = 5
)

func genMatrix() [8][5]int {
	var matrix [x][y]int
	for i := 0; i < x; i++ {
		for j := 0; j < y; j++ {
			matrix[i][j] = rand.Intn(100)
		}
	}
	return matrix
}

func main() {
	A := genMatrix()
	B := genMatrix()
	var C [x][y]int
	for i := 0; i < x; i++ {
		for j := 0; j < y; j++ {
			C[i][j] = A[i][j] + B[i][j]
		}
	}
	fmt.Println(C)
}
