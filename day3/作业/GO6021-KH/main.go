package main

import (
	"fmt"
	"math/rand"
	"strconv"
)

/*
练习题:
1. 随机初始化两个8*5的矩阵，求两个矩阵的和（逐元素相加）
2. 给定月份，判断属于哪个季节。分别用if和switch实现
3. 创建一个student结构体，包含姓名和语数外三门课的成绩。用一个slice容纳一个班的同学，求每位同学的平均分和整个班三门课的平均分，全班同学平均分低于60的有几位
*/

// 习题1
// 1. 随机初始化两个8*5的矩阵，求两个矩阵的和（逐元素相加）

// func genMatrix() [8][5]int {
// 	var matrix [8][5]int
// 	for i := 0; i < 8; i++ {
// 		for j := 0; j < 5; j++ {
// 			matrix[i][j] = rand.Intn(50)
// 		}
// 	}
// 	return matrix
// }

// func main() {
// 	A := genMatrix()
// 	B := genMatrix()
// 	var C [8][5]int
// 	for i := 0; i < 8; i++ {
// 		for j := 0; j < 5; j++ {
// 			C[i][j] = A[i][j] + B[i][j]
// 		}
// 	}
// 	for i := 0; i < 8; i++ {
// 		for j := 0; j < 5; j++ {
// 			fmt.Printf("%10d ", C[i][j])
// 		}
// 		fmt.Println()
// 	}
// }

// 2. 给定月份，判断属于哪个季节。分别用if和switch实现

// func main() {
// 	var mon int
// 	fmt.Println("输入月份(1-12): ")
// 	fmt.Scanln(&mon)
// 	// if 方式
// 	if mon >= 13 {
// 		fmt.Println("输入错误")
// 	} else if mon >= 10 {
// 		fmt.Println("winter")
// 	} else if mon >= 7 {
// 		fmt.Println("autumn")
// 	} else if mon >= 4 {
// 		fmt.Println("summer")
// 	} else {
// 		fmt.Println("spring")
// 	}

// }

// 	// switch 方式
// 	switch mon {
// 	case 1, 2, 3:
// 		fmt.Println("spring")
// 	case 4, 5, 6:
// 		fmt.Println("summer")
// 	case 7, 8, 9:
// 		fmt.Println("autumn")
// 	case 10, 11, 12:
// 		fmt.Println("winter")
//  default:
//      fmt.Println("月份输入错误")
// 	}

// }

// 3. 创建一个student结构体，包含姓名和语数外三门课的成绩。用一个slice容纳一个班的同学，求每位同学的平均分和整个班三门课的平均分，全班同学平均分低于60的有几位
type Student struct {
	Name    string
	Chinese float32
	Math    float32
	English float32
}

var sumChinese float32 = 0.0
var sumMath float32 = 0.0
var sumEnglish float32 = 0.0
var lessThen60 int = 0

func genStudentInfo(n int) {
	student := Student{}
	// fmt.Printf("%T\n", 1)
	for i := 1; i <= n; i++ {
		student.Name = strconv.Itoa(i) + "k"
		student.Chinese = float32(rand.Intn(100)) + rand.Float32()
		student.Math = float32(rand.Intn(100)) + rand.Float32()
		student.English = float32(rand.Intn(100)) + rand.Float32()

		sumChinese += student.Chinese
		sumMath += student.Math
		sumEnglish += student.English

		// 每位同学的平均分
		personalAvgSco := (student.Chinese + student.Math + student.English) / 3
		fmt.Printf("学生%s 的平均分是%v\n", student.Name, personalAvgSco)

		// 全班同学平均分低于60分
		if personalAvgSco < 60 {
			lessThen60 += 1
		}

	}
	// 整个班三门课的平均分
	fmt.Println(sumChinese, sumMath, sumEnglish)
	avgChinese := sumChinese / float32(n)
	fmt.Printf("全班语文平均分为 %f \n", avgChinese)
	avgMath := sumMath / float32(n)
	fmt.Printf("全班数学平均分为 %f \n", avgMath)
	avgEnglish := sumEnglish / float32(n)
	fmt.Printf("全班英语平均分为 %f \n", avgEnglish)

	// 全班同学平均分低于60分
	fmt.Printf("全班同学平均分低于60分的人数是 %d\n", lessThen60)

}

func main() {
	genStudentInfo(50)
}
// 第一题的变量命令要和第三题一样，尽量使用有意义的变量名，生产上随意使用A, B, C这样的变量
// 命令，代码通不过审核的。
