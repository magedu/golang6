package main

import (
	"fmt"
	"math/rand"
	"time"
)

const (                                                  //矩阵的维度
	row    = 8
	column = 5
)

func main() {
	var matrix, matrix1, matrix2 [row][column]int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < row; i++ {
		for j := 0; j < column; j++ {
			matrix1[i][j] = rand.Intn(100)
			matrix2[i][j] = rand.Intn(100)
			matrix[i][j] = matrix1[i][j] + matrix2[i][j]   //两个矩阵的和
		}
	}
	fmt.Println("matrix:")
	for _, v := range matrix {
		fmt.Println(v)
	}
}

