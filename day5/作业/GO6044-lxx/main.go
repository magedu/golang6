package main

import (
	"bytes"
	"container/list"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

// User is ...
type User struct {
	Name string
	Age  int
	Sex  byte `json:"gender"`
}

// Book is ...
type Book struct {
	ISBN     string `json:"isbn"`
	Name     string
	Price    float32      `json:"price"`
	Author   *User        `json:"author"` //把指针去掉试试
	Keywords []string     `json:"kws"`
	Local    map[int]bool //TODO 暂不支持map
}

//SplitJSON is func to split JSON string and convert to a stringslice
//由于json字符串里存在{}[]等嵌套情况，直接按,分隔是不合适的
//unmarshal 中使用的时候,先去除了最外层的括号,所以参数提到的 json,其实并不是完全需要是 json.
func SplitJSON(json string) []string {
	rect := make([]string, 0, 10)
	stack := list.New() //list是双端队列，用它来模拟栈
	beginIndex := 0
	for i, r := range json {
		// fmt.Printf("%#v", string(r))

		if r == rune('{') || r == rune('[') {
			// stack.PushBack(struct{}{}) // 这个压栈压入了一个匿名空结构体, 我们不关心栈里是什么，只关心栈里有没有元素
			stack.PushBack(1) // 压栈, 我们不关心栈里是什么，只关心栈里有没有元素
		} else if r == rune('}') || r == rune(']') {
			ele := stack.Back()
			if ele != nil {
				stack.Remove(ele) //出栈, 删除栈顶元素
			}
		} else if r == rune(',') {
			if stack.Len() == 0 { //栈为空时才可以按,分隔
				rect = append(rect, json[beginIndex:i])
				beginIndex = i + 1
			}
		}

		// fmt.Printf("--stack len %d\n", stack.Len())
		// fmt.Printf("======JSON splited result: %T  %#v\n", rect, rect)

	}
	rect = append(rect, json[beginIndex:])
	// fmt.Printf("======JSON splited result: %T  %#v\n", rect, rect)
	return rect
}

// Marshal is ...
func Marshal(v interface{}) ([]byte, error) {
	value := reflect.ValueOf(v)
	typ := value.Type() //跟typ := reflect.TypeOf(v)等价
	if typ.Kind() == reflect.Ptr {
		if value.IsNil() { //如果指向nil，直接输出null
			return []byte("null"), nil
		} //如果传的是指针类型，先解析指针
		typ = typ.Elem()
		value = value.Elem()
	}
	bf := bytes.Buffer{} //存放序列化结果
	switch typ.Kind() {
	case reflect.String:
		return []byte(fmt.Sprintf("\"%s\"", value.String())), nil //取得reflect.Value对应的原始数据的值
	case reflect.Bool:
		return []byte(fmt.Sprintf("%t", value.Bool())), nil
	case reflect.Float32,
		reflect.Float64:
		return []byte(fmt.Sprintf("%f", value.Float())), nil
	case reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64:
		return []byte(fmt.Sprintf("%v", value.Interface())), nil
	case reflect.Slice:
		// if value.IsValid() && value.IsNil() {
		// 	return []byte("null"), nil
		// }
		bf.WriteByte('[')
		if value.Len() > 0 {
			for i := 0; i < value.Len(); i++ { //取得slice的长度
				bs, err := Marshal(value.Index(i).Interface())
				if err != nil { //对slice的第i个元素进行序列化。递归
					return nil, err
				}
				bf.Write(bs)
				bf.WriteByte(',')
			}
			bf.Truncate(len(bf.Bytes()) - 1) //删除最后一个逗号
		}
		bf.WriteByte(']')
		return bf.Bytes(), nil
	case reflect.Struct:
		bf.WriteByte('{')
		if value.NumField() > 0 {
			for i := 0; i < value.NumField(); i++ {
				fieldValue := value.Field(i)
				fieldType := typ.Field(i)
				name := fieldType.Name //如果没有json Tag，默认使用成员变量的名称
				if len(fieldType.Tag.Get("json")) > 0 {
					name = fieldType.Tag.Get("json")
				}
				bf.WriteString("\"")
				bf.WriteString(name)
				bf.WriteString("\"")
				bf.WriteString(":")
				bs, err := Marshal(fieldValue.Interface())
				if err != nil { //对value递归调用Marshal序列化
					return nil, err
				}
				bf.Write(bs)
				bf.WriteString(",")
			}
			bf.Truncate(len(bf.Bytes()) - 1) //删除最后一个逗号
		}
		bf.WriteByte('}')
		return bf.Bytes(), nil
	case reflect.Map:
		bf.WriteByte('{')
		if value.Len() > 0 {
			for _, k := range value.MapKeys() {
				fieldValue := value.MapIndex(k)
				fieldType := k
				name := fieldType.Interface()
				bf.WriteString("\"")
				bf.WriteString(fmt.Sprintf("%v", name))
				bf.WriteString("\"")
				bf.WriteString(":")
				bs, err := Marshal(fieldValue.Interface())
				if err != nil { //对value递归调用Marshal序列化
					return nil, err
				}
				bf.Write(bs)
				bf.WriteString(",")
			}
			bf.Truncate(len(bf.Bytes()) - 1) //删除最后一个逗号
		}
		bf.WriteByte('}')
		return bf.Bytes(), nil
	default:
		return []byte(fmt.Sprintf("\"暂不支持该数据类型:%s\"", typ.Kind().String())), nil
	}
}

// Unmarshal string to some type's interface
// jugement the type by v interface(), and range the string
func Unmarshal(data []byte, v interface{}) error {
	s := string(data)
	//去除前后的连续空格
	s = strings.Trim(s, " ")
	if len(s) == 0 {
		return nil
	}
	typ := reflect.TypeOf(v)
	value := reflect.ValueOf(v)
	if typ.Kind() != reflect.Ptr { //因为要修改v，必须传指针
		return errors.New("must pass pointer parameter")
	}

	typ = typ.Elem() //解析指针
	value = value.Elem()

	switch typ.Kind() {
	case reflect.String:
		if s[0] == '"' && s[len(s)-1] == '"' {
			value.SetString(s[1 : len(s)-1]) //去除前后的""
		} else {
			// return errors.New(fmt.Sprintf("invalid json part: %s", s))
			return fmt.Errorf("invalid json part: %s", s)
		}
	case reflect.Bool:
		if b, err := strconv.ParseBool(s); err == nil {
			value.SetBool(b)
		} else {
			return err
		}
	case reflect.Float32,
		reflect.Float64:
		if f, err := strconv.ParseFloat(s, 64); err != nil {
			return err
		} else {
			value.SetFloat(f) //通过reflect.Value修改原始数据的值
		}
	case reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64:
		if i, err := strconv.ParseInt(s, 10, 64); err != nil {
			return err
		} else {
			value.SetInt(i) //有符号整型通过SetInt
		}
	case reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64:
		if i, err := strconv.ParseUint(s, 10, 64); err != nil {
			return err
		} else {
			value.SetUint(i) //无符号整型需要通过SetUint
		}
	case reflect.Slice:
		if s[0] == '[' && s[len(s)-1] == ']' {
			arr := SplitJSON(s[1 : len(s)-1]) //去除前后的[]
			if len(arr) > 0 {
				// slice := reflect.ValueOf(v).Elem()                    //别忘了，v是指针
				// slice.Set(reflect.MakeSlice(typ, len(arr), len(arr))) //通过反射创建slice
				value.Set(reflect.MakeSlice(typ, len(arr), len(arr))) //通过反射创建slice
				for i := 0; i < len(arr); i++ {
					eleValue := value.Index(i)
					eleType := eleValue.Type()
					if eleType.Kind() != reflect.Ptr { //和 slice 类型有关,所以需要判断是否为指针.
						eleValue = eleValue.Addr() //Elem
					}
					if err := Unmarshal([]byte(arr[i]), eleValue.Interface()); err != nil {
						return err
					}
				}
			}
		} else if s != "null" {
			return fmt.Errorf("invalid json part: %s", s)
		}
	case reflect.Struct:
		if s[0] == '{' && s[len(s)-1] == '}' {
			arr := SplitJSON(s[1 : len(s)-1])
			if len(arr) > 0 {
				fieldCount := typ.NumField()
				//建立json tag到FieldName的映射关系
				tag2Field := make(map[string]string, fieldCount)
				for i := 0; i < fieldCount; i++ {
					fieldType := typ.Field(i)
					name := fieldType.Name
					if len(fieldType.Tag.Get("json")) > 0 {
						name = fieldType.Tag.Get("json")
					}
					tag2Field[name] = fieldType.Name
				}

				for _, ele := range arr {
					brr := strings.SplitN(ele, ":", 2) //json的value里可能存在嵌套，所以用:分隔时限定个数为2
					if len(brr) == 2 {
						tag := strings.Trim(brr[0], " ")
						if tag[0] == '"' && tag[len(tag)-1] == '"' { //json的key肯定是带""的
							tag = tag[1 : len(tag)-1]                        //去除json key前后的""
							if fieldName, exists := tag2Field[tag]; exists { //根据json key(即json tag)找到对应的FieldName
								fieldValue := value.FieldByName(fieldName)
								fieldType := fieldValue.Type()
								if fieldType.Kind() != reflect.Ptr {
									//如果内嵌不是指针，则声明时已经用0值初始化了，此处只需要根据json改写它的值
									fieldValue = fieldValue.Addr()                                            //确保fieldValue指向指针类型，因为接下来要把fieldValue传给Unmarshal
									if err := Unmarshal([]byte(brr[1]), fieldValue.Interface()); err != nil { //递归调用Unmarshal，给fieldValue的底层数据赋值
										return err
									}
								} else {
									//如果内嵌的是指针，则需要通过New()创建一个实例(申请内存空间)。不能给New()传指针型的Type，所以调一下Elem()
									newValue := reflect.New(fieldType.Elem())                               //newValue代表的是指针
									if err := Unmarshal([]byte(brr[1]), newValue.Interface()); err != nil { //递归调用Unmarshal，给fieldValue的底层数据赋值
										return err
									}
									value.FieldByName(fieldName).Set(newValue) //把newValue赋给value的Field
								}

							} else {
								fmt.Printf("字段%s找不到\n", tag)
							}
						} else {
							return fmt.Errorf("invalid json part: %s", tag)
						}
					} else {
						return fmt.Errorf("invalid json part: %s", ele)
					}
				}
			}
		} else if s != "null" {
			return fmt.Errorf("invalid json part: %s", s)
		}
	case reflect.Map:
		if s == "null" {
			return fmt.Errorf("invalid json part: %s", s)
		}

		if s[0] == '{' && s[len(s)-1] == '}' {
			arr := SplitJSON(s[1 : len(s)-1])

			// mapInst := reflect.ValueOf(v).Elem()                //别忘了，v是指针, Unmarsal 传参的时候传入指针
			// mapInst.Set(reflect.MakeMapWithSize(typ, len(arr))) //通过反射创建map
			value.Set(reflect.MakeMapWithSize(typ, len(arr)))

			// 获取 map 的 k, v 类型, 并创建有一个该类型的 value. 用于 unmarsul 传参
			kType := typ.Key()
			kValue := reflect.New(kType)
			vType := typ.Elem()
			vValue := reflect.New(vType)

			// fmt.Println(typ, kType, vType)

			if len(arr) > 0 { // 如果 arr 为 0, 则不处理. 直接返回初始化的空 map

				for _, ele := range arr { // 遍历 被分割成数组的 json 字符串, 每一个 ele 是一个 map k:v 字符串.
					brr := strings.SplitN(ele, ":", 2) //json的value里可能存在嵌套，所以用:分隔时限定个数为2, 根据第一个: 将字符串分割为 2 段
					if len(brr) == 2 {
						k := strings.Trim(brr[0], " ")
						v := strings.Trim(brr[1], " ")
						if k[0] != '"' && k[len(k)-1] != '"' { //json的key肯定是带""的
							return fmt.Errorf("invalid json part: %s", k)
						}

						k = strings.Trim(k, "\"")
						v = strings.Trim(v, "\"")

						// fmt.Println(brr, k, v)

						if err := Unmarshal([]byte(k), kValue.Interface()); err != nil { // 使用 Unmarshal 给 kValue 赋值
							return err
						}

						if err := Unmarshal([]byte(v), vValue.Interface()); err != nil { // 使用 Unmarshal 给 vValue 赋值
							return err
						}

						// fmt.Println(kValue.Elem(), vValue.Elem())
						value.SetMapIndex(kValue.Elem(), vValue.Elem()) // 使用 SetMapIndex 给 Map 赋值

					} else {
						return fmt.Errorf("invalid json part: %s", ele)
					}
				}
			}
		}
	default:
		fmt.Printf("暂不支持类型:%s\n", typ.Kind().String())
	}
	return nil
}

func main() {
	user := User{
		Name: "钱钟书",
		Age:  57,
		Sex:  1,
	}
	book := Book{
		ISBN:     "4243547567",
		Name:     "围城",
		Price:    34.8,
		Author:   &user,                      //改成nil试试
		Keywords: []string{"爱情", "民国", "留学"}, //把这一行注释掉试一下，测测null
		Local:    map[int]bool{2: true, 3: false},
	}

	if bytes, err := Marshal(user); err != nil { //也可以给Marshal传指针类型
		fmt.Printf("序列化失败: %v\n", err)
	} else {
		fmt.Println(string(bytes))
		var u User
		if err = Unmarshal(bytes, &u); err != nil {
			fmt.Printf("反序列化失败: %v\n", err)
		} else {
			// fmt.Printf("user name %s\n", u.Name)
			fmt.Printf("user unmarshal %T %#v\n", u, u)
		}
	}

	// if bytes, err := json.Marshal(user); err != nil {
	// 	fmt.Printf("序列化失败: %v\n", err)
	// } else {
	// 	fmt.Println(string(bytes))
	// 	var u User
	// 	if err = json.Unmarshal(bytes, &u); err != nil {
	// 		fmt.Printf("反序列化失败: %v\n", err)
	// 	} else {
	// 		fmt.Printf("user name %s\n", u.Name)
	// 	}
	// }

	if bytes, err := Marshal(book); err != nil { //也可以给Marshal传指针类型
		fmt.Printf("序列化失败: %v\n", err)
	} else {
		fmt.Println(string(bytes))
		var b Book //必须先声明值类型，再通过&给Unmarshal传一个指针参数。因为声明值类型会初始化为0值，而声明指针都没有创建底层的内存空间
		if err = Unmarshal(bytes, &b); err != nil {
			fmt.Printf("反序列化失败: %v\n", err)
		} else {
			// fmt.Printf("book name %s author name %s local %v\n", b.Name, b.Author.Name, b.Local)
			fmt.Printf("%#v \n", b)
			fmt.Printf("%#v \n", b.Author)
		}
	}

	// if bytes, err := json.Marshal(book); err != nil {
	// 	fmt.Printf("序列化失败: %v\n", err)
	// } else {
	// 	fmt.Println(string(bytes))
	// 	var b Book
	// 	if err = json.Unmarshal(bytes, &b); err != nil {
	// 		fmt.Printf("反序列化失败: %v\n", err)
	// 	} else {
	// 		// fmt.Printf("book name %s author name %s local %v\n", b.Name, b.Author.Name, b.Local)
	// 		fmt.Printf("book unmarshal %T %v#\n", b, b)
	// 	}
	// }
}
