/*
把字符串1998-10-01 08:10:00解析成time.Time，再格式化成字符串199810010810
*/

package main

import (
	"fmt"
	"time"
)

const (
	timeStr = "1998-10-01 08:10:00"
)

func main() {
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		fmt.Printf("load location failed! err: %v\n", err)
		return
	}
	timeParse, err := time.ParseInLocation("2006-01-02 15:04:05", timeStr, loc)
	if err != nil {
		fmt.Printf("parse time failed! err: %v\n", err)
		return
	}
	fmt.Println(timeParse)

	timeFormatStr := timeParse.Format("200601021504")
	fmt.Println(timeFormatStr)

}
