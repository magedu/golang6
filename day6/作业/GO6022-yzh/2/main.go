/*
我们是每周六上课，输出我们未来4次课的上课日期（不考虑法定假日）
*/

package main

import (
	"fmt"
	"time"
)

func getFuture(t time.Time) (time.Time, error) {
	if t.Weekday() != 6 {
		days := 6 - t.Weekday()
		t = t.Add(time.Duration(days) * 24 * time.Hour)
	}
	return t.Add(7 * 24 * time.Hour), nil
}

func main() {
	t := time.Now()
	var err error
	for i := 0; i < 4; i++ {
		t, err = getFuture(t)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(t)
	}
}
