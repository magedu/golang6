package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"time"
)


// 第一题：把字符串1998-10-01 08:10:00解析成time.Time，再格式化成字符串199810010810
func formatTime(t string) (string){
	// 加载时区
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		fmt.Println(err)
	}
	// 解析成time.time格式
	timeObj, err := time.ParseInLocation("2006-01-02 15:04:05", t, loc)
	if err != nil {
		fmt.Println(err)
	}
	// 格式化成字符串199810010810
	fmt.Println(timeObj.Format("200601020304"))
	return timeObj.Format("200601020304")
}

// 第二题：我们是每周六上课，输出我们未来4次课的上课日期（不考虑法定假日）
func timetable(time2 time.Time, n int) []time.Time {
	classSchedule := make([]time.Time, n+1)

	//初始上课时间
	classSchedule[0] = time2
	for i := 0; i < n; i++ {
		classSchedule[i+1] = classSchedule[i].Add(time.Hour * 24 * 7)
	}
	return classSchedule
}

// 第三题：把一个目录下的所有.txt文件合一个大的.txt文件，再对这个大文件进行压缩
func fileMerge(filePath string)  {
	// 获取目录下的所有文件
	files, err := ioutil.ReadDir(filePath)
	if err != nil {
		return
	}

	// 获取当前目录下的所有.txt的文件
	var allFile []string
	for _, file := range files {
		if !file.IsDir() {
			fullPath := filePath + "/" + file.Name()
			if path.Ext(fullPath) == ".txt" {
				allFile = append(allFile, fullPath)
			}
		}
	}

	// 合并文件
	fileMerge, err := os.OpenFile(filePath + "/fileMerge.txt", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("open file failed, err:", err)
		return
	}
	defer fileMerge.Close()
	writer := bufio.NewWriter(fileMerge)

	for _, s := range allFile {
		file, err := os.Open(s)
		if err != nil {
			fmt.Println("open file failed, err:", err)
			return
		}
		defer file.Close()

 		// 按行读取文件内容，追加到合并后的文件
		reader := bufio.NewReader(file)
		for {
			line, err := reader.ReadString('\n') //注意是字符
			if err == io.EOF {
				if len(line) != 0 {
					writer.WriteString(line + "\n")
					writer.Flush()
				}
				break
			}
			if err != nil {
				fmt.Println("read file failed, err:", err)
				return
			}
			writer.WriteString(line)
			writer.Flush()
		}
	}

	// 压缩文件
}

func main() {
	// 第一题：把字符串1998-10-01 08:10:00解析成time.Time，再格式化成字符串199810010810
	formatTime("1998-10-01 08:10:00")

	//第二题：我们是每周六上课，输出我们未来4次课的上课日期（不考虑法定假日）
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		fmt.Println(err)
	}
	// 第1次上课时间
	classTime, err := time.ParseInLocation("2006-01-02 15:04", "2021-11-06 09:00", loc)
	// 遍历4次上课时间
	for i, t := range timetable(classTime, 4) {
		fmt.Printf("第%d次课时间：%v\n" , i, t.Format("2006-01-02 03:04"))
	}

	// 第三题：把一个目录下的所有.txt文件合一个大的.txt文件，再对这个大文件进行压缩
	fileMerge("./day6/作业/GO6012-GalabashBrothers/files")


}