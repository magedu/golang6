package main

import (
	"compress/gzip"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

/*
把字符串1998-10-01 08:10:00解析成time.Time，再格式化成字符串199810010810
我们是每周六上课，输出我们未来4次课的上课日期（不考虑法定假日）
把一个目录下的所有.txt文件合一个大的.txt文件，再对这个大文件进行压缩
*/

var (
	file_slice = make([]string, 0, 10)
)

func Homework1() {
	timestring := "1998-10-01 08:10:00"
	timetime, _ := time.Parse("2006-01-02 15:04:05", timestring)
	fmt.Println(timetime)
	timestring = timetime.Format("20060102150405")
	fmt.Println(timestring)
}
func Homework2() {
	timestring := "2021-10-30"
	timetime, _ := time.Parse("2006-01-02", timestring)
	for i := 1; i < 5; i++ {
		fmt.Println(timetime.AddDate(0, 0, i*7))
	}

}

func Homework3(o string) {
	if fileinfors, err := ioutil.ReadDir(o); err != nil {
		fmt.Println(err)
	} else {
		for _, fileinfo := range fileinfors {
			if fileinfo.IsDir() {
				Homework3(filepath.Join(o, fileinfo.Name()))
			} else {
				filestring := fileinfo.Name()
				if strings.HasSuffix(filestring, ".txt") {
					file_slice = append(file_slice, filepath.Join(o, filestring))
				}
			}
		}
	}
	if ouputfile, err := os.Create(filepath.Join(o, "output.gz")); err != nil {
		fmt.Println(err)
	} else {
		write := gzip.NewWriter(ouputfile)
		for _, file := range file_slice {
			if file_byte, err := ioutil.ReadFile(file); err != nil {
				fmt.Println(err)
			} else {
				write.Write(file_byte)
			}
		}
		ouputfile.Close()
		write.Close()
		// gzipFile, _ := os.Open("C:\\Users\\Administrator\\Desktop\\pinginfoview\\output.gz")
		// gzipReader, _ := gzip.NewReader(gzipFile)
		// defer gzipReader.Close()
		// outfileWriter, err := os.Create("C:\\Users\\Administrator\\Desktop\\pinginfoview\\unzipped.txt")
		// if err != nil {
		// 	log.Fatal(err)
		// }
		// defer outfileWriter.Close()
		// 复制内容
		// fmt.Println("aaaaa", outfileWriter, gzipFile)
		// _, err = io.Copy(os.Stdout, gzipReader)
		// if err != nil {
		// 	log.Fatal(err)
		// }
	}
	fmt.Println(file_slice)

}
func main() {
	// Homework1()
	// Homework2()
	Homework3("C:\\Users\\Administrator\\Desktop\\pinginfoview")

}
