package main

import (
	"bufio"
	"bytes"
	"compress/zlib"
	"fmt"
	"os"
	"path"
	"time"
)

func main() {
// homework1 指定时间格式进行输出
	OLD_TF := "2006-01-02 15:04:05"
	//解析时间
	t, err := time.Parse(OLD_TF, "1998-10-01 08:10:00")
	if err != nil {
		fmt.Println("Time Format Error")
	}
	//自定义时间格式
	NEW_TF := "20060102150405"
	fmt.Println(t.Format(NEW_TF))

// homework2 未来四天的上课时间
	LastWeek := time.Date(2021, 10, 30, 9, 0, 0, 0, time.Local) //上次上课时间
	for i := 0; i < 4; i++ {
		NextWeek := LastWeek.AddDate(0,0,7).Format(OLD_TF) //格式化时间
		fmt.Println(NextWeek) //输出未来四周上课时间
	}

// homework3 压缩某个目录下的所有.txt结尾的文件

	//读取当前目录中的所有文件.
	if files, err := os.ReadDir("."); err != nil {
		fmt.Println(err)
	} else {
		
		//创建一个writer
		var AllFile bytes.Buffer
		writer := bufio.NewWriter(&AllFile)


		//逐个处理文件
		for _, file := range files {
			fname := file.Name()
			//跳过不是 .txt 后缀的文件
			if path.Ext(fname) != ".txt" {
				continue
			}
			
			//读取每个文件
			temp, err := os.ReadFile(fname)
			if err != nil {
				fmt.Println(err)
			}
			//然后写入Writer
			if _, err := writer.Write(temp); err != nil {
				fmt.Println(err)
			}
		}
		writer.Flush() //刷新缓冲区

		//创建压缩文件
		if cmpfile,err := os.Create("AllFile_Compress"); err != nil {
			fmt.Println(err)
		} else {
			//将合并后的所有字节进行压缩
			CmpWriter := zlib.NewWriter(cmpfile)
			if _, err := CmpWriter.Write(AllFile.Bytes()); err != nil {
				fmt.Println(err)
			}
			CmpWriter.Close()
			cmpfile.Close()
		}
	}
}
