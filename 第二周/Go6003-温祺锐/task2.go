package main

import (
	"fmt"
	"strconv"
	"strings"
)

func arr2string(arr []int) string {
	str := strings.Builder{}
	for _, val := range arr {
		str.WriteString(strconv.Itoa(val))
        str.WriteString(" ")
	}
	return str.String()
}

func main() {
	sli := []int{1,2,3,4}
	fmt.Println(arr2string(sli))
}