package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

//1.创建一个初始长度为0、容量为10的int型切片，调用rand.Intn(128)100次，往切片里面添加100个元素，利用map统计该切片里有多少个互不相同的元素。
func homework1() {
	var myslice []int
	myslice = make([]int, 0, 10)

	for i := 1; i <= 100; i++ {
		// 调用rand.Intn(128)100次，往切片里面添加100个元素
		num := fmt.Sprintf("%d", rand.Intn(128))
		appnum, _ := strconv.Atoi(num)
		myslice = append(myslice, appnum)
	}

	mymap := make(map[int]int)
	for _, v := range myslice {
		//将slice值作为map的key添加到至mymap中
		mymap[v] = 0
	}

	fmt.Println(len(mymap)) //计算mymap的长度
}

//2.实现一个函数func arr2string(arr []int) string，比如输入[]int{2,4,6}，返回“2 4 6”。输入的切片可能很短，也可能很长。
func arr2string(arr []int) string {
	var mystr strings.Builder
	for _, str := range arr {
		newstr := strconv.Itoa(str)
		mystr.WriteString(newstr + " ")
	}
	return mystr.String()
}

func main() {
	//第一题
	homework1()

	//第二题
	var arr []int = []int{1, 2, 3, 4, 5}
	homework2 := arr2string(arr)
	fmt.Println(homework2)
}
